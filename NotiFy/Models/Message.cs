﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CriptExchNotifys.Models
{
    public class Message
    {
        public string Type { get; set; }
        public string Payload { get; set; }
        public int id { get; set; }
        public string asunto { get; set; }
        public string ex { get; set; }
        public string estado { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> id_usuario { get; set; }
        public int id_estado { get; set; }
        public Nullable<System.DateTime> fecha_sistema { get; set; }
        public string url { get; set; }
        public Nullable<int> id_general { get; set; }
    }
}
