﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CriptExchNotifys.Models
{
    public interface ITypedHubClient
    {
        Task BroadcastMessage(string type, string payload, string url, string asunto, string ex, string estado, string descripcion, int id_usuario, int id_estado, DateTime fecha_sistema, int id_general);
    }
}