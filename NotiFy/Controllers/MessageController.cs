﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CriptExchNotifys.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace CriptExchNotifys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private IHubContext<NotifyHub, ITypedHubClient> _hubContext;

        public MessageController(IHubContext<NotifyHub, ITypedHubClient> hubContext)
        {
            _hubContext = hubContext;
        }

        [HttpPost]
        public string Post([FromBody]Message msg)
        {
            string retMessage;

            try
            {
                _hubContext.Clients.All.BroadcastMessage(msg.Type, msg.Payload, msg.url, msg.asunto, msg.ex, msg.estado, msg.descripcion, Convert.ToInt32(msg.id_usuario), msg.id_estado, Convert.ToDateTime(msg.fecha_sistema), Convert.ToInt32(msg.id_general));
                retMessage = "Completado";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }

            return retMessage;
        }
    }
}